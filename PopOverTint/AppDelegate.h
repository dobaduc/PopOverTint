//
//  AppDelegate.h
//  PopOverTint
//
//  Created by Duc DoBa on 9/12/15.
//  Copyright © 2015 Ducky Duke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

