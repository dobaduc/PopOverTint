//
//  ViewController.m
//  PopOverTint
//
//  Created by Duc DoBa on 9/12/15.
//  Copyright © 2015 Ducky Duke. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.button1 setImage:[[UIImage imageNamed:@"trash"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                  forState:UIControlStateNormal];
    self.button1.tintColor = [UIColor purpleColor];
    
    self.button2.tintColor = [UIColor blueColor];
}

@end



