//
//  main.m
//  PopOverTint
//
//  Created by Duc DoBa on 9/12/15.
//  Copyright © 2015 Ducky Duke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
